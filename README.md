# nRF9160 LTE Power Analysis

This repo holds the code used to analyse power usage of the nRF9160 using different protocols.

## How to clone this repo

To clone run the following command:

```bash
west init -m git@gitlab.com:oskaro/nrf9160-lte-power-analysis.git nrf9160-lte-power-analysys
west update
pip install -r zephyr/scripts/requirements.txt
pip install -r nrf/scripts/requirements.txt
pip install -r bootloader/mcuboot/scripts/requirements.txt
source zephyr/zephyr-env.sh
cd app
west build
```

This requires the GNU toolchain to be installed and the toolchain variables to be exported in `~/.zephyrrc` as follows:

```bash
export ZEPHYR_TOOLCHAIN_VARIANT=gnuarmemb
export GNUARMEMB_TOOLCHAIN_PATH="/path/to/toolchain"
```
