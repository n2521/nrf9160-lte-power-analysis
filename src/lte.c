#include "lte.h"

#include <string.h>
#include <stdio.h>
#include <dk_buttons_and_leds.h>
#include <zephyr.h>
#include <logging/log.h>
#include <random/rand32.h>
#include <modem/lte_lc.h>
#include <net/coap.h>
#include <net/socket.h>

LOG_MODULE_REGISTER(lte, LOG_LEVEL_DBG);

K_SEM_DEFINE(lte_connected, 0, 1);

K_SEM_DEFINE(rrc_active, 0, 1);
K_SEM_DEFINE(rrc_inactive, 0, 1);
K_SEM_DEFINE(user_forced_rrc, 0, 1);

atomic_t rrc_mode = ATOMIC_INIT(0);

#define LED_RRC_ACTIVE    DK_LED1

#define APP_COAP_SEND_INTERVAL_MS 30000
#define APP_COAP_MAX_MSG_LEN      1280
#define APP_COAP_VERSION          1

static int sock;
static struct pollfd fds;
static struct sockaddr_storage server;
static uint16_t next_token;

static uint8_t coap_buf[APP_COAP_MAX_MSG_LEN];
static uint8_t dummy_payload[CONFIG_USER_PAYLOAD_SIZE] = {0};

#if defined(CONFIG_NRF_MODEM_LIB)

/**@brief Recoverable modem library error. */
void nrf_modem_recoverable_error_handler(uint32_t err) {
    LOG_INF("Modem library recoverable error: %u", (unsigned int)err);
}

#endif /* defined(CONFIG_NRF_MODEM_LIB) */

/**@brief Resolves the configured hostname. */
static int server_resolve(void) {
    int err;
    struct addrinfo *result;
    struct addrinfo hints = {.ai_family = AF_INET, .ai_socktype = SOCK_DGRAM};
    char ipv4_addr[NET_IPV4_ADDR_LEN];

    err = getaddrinfo(CONFIG_COAP_SERVER_HOSTNAME, NULL, &hints, &result);
    if (err != 0) {
        LOG_INF("ERROR: getaddrinfo failed %d", err);
        return -EIO;
    }

    if (result == NULL) {
        LOG_INF("ERROR: Address not found");
        return -ENOENT;
    }

    /* IPv4 Address. */
    struct sockaddr_in *server4 = ((struct sockaddr_in *)&server);

    server4->sin_addr.s_addr =
        ((struct sockaddr_in *)result->ai_addr)->sin_addr.s_addr;
    server4->sin_family = AF_INET;
    server4->sin_port = htons(CONFIG_COAP_SERVER_PORT);

    inet_ntop(AF_INET, &server4->sin_addr.s_addr, ipv4_addr, sizeof(ipv4_addr));
    LOG_INF("IPv4 Address found %s", log_strdup(ipv4_addr));

    /* Free the address. */
    freeaddrinfo(result);

    return 0;
}

/**@brief Initialize the CoAP client */
static int client_init(void) {
    int err;

    sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sock < 0) {
        LOG_INF("Failed to create CoAP socket: %d.", errno);
        return -errno;
    }

    err = connect(sock, (struct sockaddr *)&server, sizeof(struct sockaddr_in));
    if (err < 0) {
        LOG_INF("Connect failed : %d", errno);
        return -errno;
    }

    /* Initialize FDS, for poll. */
    fds.fd = sock;
    fds.events = POLLIN;

    /* Randomize token. */
    next_token = sys_rand32_get();

    return 0;
}

/**@brief Handles responses from the remote CoAP server. */
static int client_handle_get_response(uint8_t *buf, int received) {
    int err;
    struct coap_packet reply;
    const uint8_t *payload;
    uint16_t payload_len;
    uint8_t token[8];
    uint16_t token_len;
    uint8_t temp_buf[16];

    err = coap_packet_parse(&reply, buf, received, NULL, 0);
    if (err < 0) {
        LOG_INF("Malformed response received: %d", err);
        return err;
    }

    payload = coap_packet_get_payload(&reply, &payload_len);
    token_len = coap_header_get_token(&reply, token);

    if ((token_len != sizeof(next_token)) &&
        (memcmp(&next_token, token, sizeof(next_token)) != 0)) {
        LOG_INF("Invalid token received: 0x%02x%02x", token[1], token[0]);
        return 0;
    }

    snprintf(temp_buf, MAX(payload_len, sizeof(temp_buf)), "%s", payload);

    LOG_INF("CoAP response: code: 0x%x, token 0x%02x%02x, payload: %s .",
            coap_header_get_code(&reply), token[1], token[0],
            log_strdup(temp_buf));

    return 0;
}

/**@biref Send CoAP GET request. */
static int client_get_send(void) {
    int err;
    struct coap_packet request;

    next_token++;

    err = coap_packet_init(&request, coap_buf, sizeof(coap_buf),
                           APP_COAP_VERSION, COAP_TYPE_NON_CON,
                           sizeof(next_token), (uint8_t *)&next_token,
                           COAP_METHOD_GET, coap_next_id());
    if (err < 0) {
        LOG_ERR("Failed to create CoAP request, %d", err);
        return err;
    }

    err = coap_packet_append_option(&request, COAP_OPTION_URI_PATH,
                                    (uint8_t *)CONFIG_COAP_RESOURCE,
                                    strlen(CONFIG_COAP_RESOURCE));
    if (err < 0) {
        LOG_ERR("Failed to encode CoAP option, %d", err);
        return err;
    }

    err = coap_packet_append_payload_marker(&request);
    if (err < 0) {
        LOG_ERR("Failed to append payload marker. (%d)", err);
        return err;
    }

    err = coap_packet_append_payload(&request, dummy_payload, sizeof(dummy_payload));
    if (err < 0) {
        LOG_ERR("Failed to append payload. (%d)", err);
        return err;
    }

    err = send(sock, request.data, request.offset, 0);
    if (err < 0) {
        LOG_INF("Failed to send CoAP request, %d", errno);
        return -errno;
    }

    LOG_INF("CoAP request sent: token 0x%04x, size: %d", next_token, request.offset);

    return 0;
}

static void lte_lc_connect_handler(const struct lte_lc_evt *const evt) {
    LOG_INF("Got event: %d", evt->type);
    switch (evt->type) {
        case LTE_LC_EVT_NW_REG_STATUS:
            if ((evt->nw_reg_status != LTE_LC_NW_REG_REGISTERED_HOME) &&
                (evt->nw_reg_status != LTE_LC_NW_REG_REGISTERED_ROAMING)) {
                LOG_INF("Got registration status: %d", evt->nw_reg_status);
                break;
            }

            LOG_INF("Connected to network: %s", evt->nw_reg_status == LTE_LC_NW_REG_REGISTERED_HOME ? "home": "roaming");

            k_sem_give(&lte_connected);
        case LTE_LC_EVT_RRC_UPDATE:
            LOG_INF("Got RRC mode: %d", evt->rrc_mode);

            k_sem_take(&rrc_active, K_NO_WAIT);
            k_sem_take(&rrc_inactive, K_NO_WAIT);

            if (k_sem_take(&user_forced_rrc, K_NO_WAIT) == 0) {
                LOG_INF("The RRC was user generated, ignoring for now");
                dk_set_led_on(LED_RRC_ACTIVE);
                return;
            }

            atomic_set(&rrc_mode, evt->rrc_mode);
            if(evt->rrc_mode == LTE_LC_RRC_MODE_CONNECTED) {
                k_sem_give(&rrc_active);
                dk_set_led_on(LED_RRC_ACTIVE);
            } else {
                k_sem_give(&rrc_inactive);
                dk_set_led_off(LED_RRC_ACTIVE);
            }

            break;
        case LTE_LC_EVT_LTE_MODE_UPDATE:
            LOG_INF("Updated mode: %d", evt->lte_mode);
            break;
        case LTE_LC_EVT_PSM_UPDATE:
            LOG_INF("Got PSM config: active_time = %d, tau = %d", evt->psm_cfg.active_time,evt->psm_cfg.tau);
            break;
        case LTE_LC_EVT_EDRX_UPDATE:
            LOG_INF("Got eDRX update, mode: %d, edrx: %.2f,  ptw: %.2f", evt->edrx_cfg.mode, evt->edrx_cfg.edrx, evt->edrx_cfg.ptw);
            break;
        default:
            break;
    }
}

static int modem_configure(void) {
    int err;
    LOG_INF("LTE Link Initializing...");
    err = lte_lc_init();
    __ASSERT(err == 0, "LTE link could not be initialized.");


    if(IS_ENABLED(CONFIG_ENABLE_PSM)) {
        LOG_INF("Trying to enable PSM");
        /*
        lte_lc_psm_param_set("01001001", "11100000");
        */
        err = lte_lc_psm_req(true);
        if(err) {
            LOG_ERR("Could not enable PSM");
            return err;
        }

    } else if (IS_ENABLED(CONFIG_ENABLE_EDRX)) {
        LOG_INF("Trying to enable eDRX");
        /*
        err = lte_lc_edrx_param_set(LTE_LC_LTE_MODE_LTEM, "0100");
        if (err) {
            LOG_ERR("Could not set eDRX params");
            return err;
        }
        */
        err = lte_lc_edrx_req(true);
        if(err) {
            LOG_ERR("Could not enable eDRX");
            return err;
        }

    }

    if(!IS_ENABLED(CONFIG_MANUAL_MODEM_SWITCHING)) {
        LOG_INF("Connecting to LTE");
        err = lte_lc_connect_async(lte_lc_connect_handler);
        if (err != 0) {
            LOG_ERR("Could not connect to LTE");
            return err;
        }

        return k_sem_take(&lte_connected, K_FOREVER);
    }
    return 0;
}

static int modem_disable() {
    if(!IS_ENABLED(CONFIG_MANUAL_MODEM_SWITCHING)) {
        LOG_DBG("No modem switching is required");
        return 0;
    }
    LOG_INF("Turning off modem");
    return lte_lc_offline();
}

static int modem_enable() {
    if(!IS_ENABLED(CONFIG_MANUAL_MODEM_SWITCHING)) {
        LOG_DBG("No modem switching is required");
        return 0;
    }
    LOG_INF("Connecting to LTE");
    int ret = lte_lc_connect_async(lte_lc_connect_handler);
    if (ret != 0) {
        return ret;
    }

    return k_sem_take(&lte_connected, K_FOREVER);
}

static int wait(int timeout) {
    int ret = poll(&fds, 1, timeout);

    if (ret < 0) {
        LOG_INF("poll error: %d", errno);
        return -errno;
    }

    if (ret == 0) {
        /* Timeout. */
        return -EAGAIN;
    }

    if ((fds.revents & POLLERR) == POLLERR) {
        LOG_INF("wait: POLLERR");
        return -EIO;
    }

    if ((fds.revents & POLLNVAL) == POLLNVAL) {
        LOG_INF("wait: POLLNVAL");
        return -EBADF;
    }

    if ((fds.revents & POLLIN) != POLLIN) {
        return -EAGAIN;
    }

    return 0;
}

static int do_coap_transmission() {
    int err, received;
    err = server_resolve();
    if (err != 0) {
        LOG_ERR("Failed to resolve server name");
        return err;
    }
    err = client_init();
    if (err != 0) {
        LOG_ERR("Failed to initialize CoAP client");
        return err;
    }

    if (client_get_send() != 0) {
        LOG_ERR("Failed to send GET request, exit...");
        return -1;
    }
    while ((err = wait(100)) == -EAGAIN) {
    }
    if (err < 0) {
        LOG_ERR("Poll error, exit... %d", err);
        return -1;
    }

    do {
        received = recv(sock, coap_buf, sizeof(coap_buf), MSG_DONTWAIT);
    } while (received < 0 && (errno == EAGAIN || errno == EWOULDBLOCK));

    if (received < 0) {
        LOG_ERR("Socket error, exit..., %d", errno);
        return -1;
    }

    if (received == 0) {
        LOG_WRN("Empty datagram");
        return 0;
    }

    err = client_handle_get_response(coap_buf, received);
    if (err < 0) {
        LOG_ERR("Invalid response, exit...");
        return -1;
    }
    (void)close(sock);
    return 0;
}

void lte_button_handler() {
    LOG_INF("Forcing RRC active");
    k_sem_give(&user_forced_rrc);
    k_sem_give(&rrc_active);
}

int lte_block_until_ready() {
    if (IS_ENABLED(CONFIG_ENABLE_EDRX) || IS_ENABLED(CONFIG_ENABLE_PSM)) {
        LOG_INF("Awaiting RRC Active period");
        k_sem_take(&rrc_active, K_FOREVER);
    } else {
        LOG_INF("Sleeping until next transmission");
        k_sleep(K_MSEC(APP_COAP_SEND_INTERVAL_MS));
    }
    return 0;
}

int lte_do_transmission() {
    int ret = modem_enable();
    if (ret < 0) {
        LOG_ERR("Could not enable modem!");
        return ret;
    }
    if (IS_ENABLED(CONFIG_TEST_COAP)) {
        LOG_INF("Doing CoAP transmission");
        if (do_coap_transmission() < 0) {
            LOG_ERR("CoAP transmission failed!");
            return -1;
        }
    } else if (IS_ENABLED(CONFIG_TEST_MQTT)) {
    } else if (IS_ENABLED(CONFIG_TEST_HTTPS)) {
    } else {
        LOG_ERR("No communications protocol enabled!");
        return -1;
    }

    ret = modem_disable();
    if (ret < 0) {
        LOG_ERR("Could not disable modem!");
        return ret;
    }
    return 0;
}

int lte_init() {
    int ret = modem_configure();
    if (ret != 0) {
        return ret;
    }
    for (int i = 0; i < sizeof(dummy_payload); ++i) {
        // fill with "ascii" letters
        dummy_payload[i] = 'c';
    }
    return 0;
}


int lte_get_rrc_mode() {
    return atomic_get(&rrc_mode);
}

int lte_await_rrc_inactive(k_timeout_t timeout) {
    return k_sem_take(&rrc_inactive, timeout);
}
