#include "gps.h"

#include <zephyr.h>

#include <nrf_modem_gnss.h>
#include <logging/log.h>
#include "sys/util_macro.h"


LOG_MODULE_REGISTER(gps, LOG_LEVEL_DBG);

K_SEM_DEFINE(gps_got_fix, 0, 1);


static bool got_fix_sucessfully = false;
static struct nrf_modem_gnss_pvt_data_frame pvt_data;

static struct nrf_modem_gnss_agps_data_location agps_location = {
    // (2**23)/90) * 63.448027
    .latitude = 5913784, 
    // (2**24) / 360 * 10.440842
    .longitude = 707583,
    .altitude = 10,
    .unc_semimajor = 127,
    .unc_semiminor = 127,
    .orientation_major = 0,
    .unc_altitude = 127,
    .confidence = 100,
};


static void gnss_event_handler(int event_id) {
    int err;
    int n_sats = 0;
    switch (event_id) {
        case NRF_MODEM_GNSS_EVT_FIX:
            LOG_INF("GOT A FIX!");
        case NRF_MODEM_GNSS_EVT_PVT:
            err = nrf_modem_gnss_read(&pvt_data, sizeof(pvt_data), NRF_MODEM_GNSS_DATA_PVT);
            if (err != 0) {
                LOG_ERR("Could not get PVT data");
                got_fix_sucessfully = false;
                k_sem_give(&gps_got_fix);
            }
            if ((pvt_data.flags & NRF_MODEM_GNSS_PVT_FLAG_NOT_ENOUGH_WINDOW_TIME)) {
                break;
            }
            if((pvt_data.flags & NRF_MODEM_GNSS_PVT_FLAG_FIX_VALID) == 0) {
                LOG_DBG("Satellites in fix:");
                for (int i = 0; i < NRF_MODEM_GNSS_MAX_SATELLITES; ++i) {
                    if (pvt_data.sv[i].flags &
                        NRF_MODEM_GNSS_SV_FLAG_USED_IN_FIX) {
                        n_sats++;
                        LOG_DBG("id: %d, CN0, %d", pvt_data.sv[i].sv, pvt_data.sv[i].cn0);
                    }
                }
                LOG_DBG("%d total satellites in use", n_sats);
                return;
            }

            k_sem_give(&gps_got_fix);
            break;

        case NRF_MODEM_GNSS_EVT_BLOCKED:
            LOG_DBG("GPS blocked");
            break;
        case NRF_MODEM_GNSS_EVT_UNBLOCKED:
            LOG_DBG("GPS unblocked");
            break;
        case NRF_MODEM_GNSS_EVT_AGPS_REQ:
            LOG_DBG("Need AGPS");
            break;
        default:
            LOG_DBG("Got GPS event: %d", event_id);
            break;
    }
}

int gps_init() {

    int err = 0;

    err = nrf_modem_gnss_init();
    if (err != 0) {
        LOG_ERR("Could not initialize GNSS: %d", err);
        return err;
    }

    err = nrf_modem_gnss_event_handler_set(gnss_event_handler);
    if (err != 0) {
        LOG_ERR("Could not set GNSS event handler: %d", err);
        return err;
    }

    if(IS_ENABLED(CONFIG_GPS_USE_AGPS)) {
        err = nrf_modem_gnss_agps_write(&agps_location, sizeof(agps_location), NRF_MODEM_GNSS_AGPS_LOCATION);
        if(err != 0) {
            LOG_ERR("Could not inject AGPS data: %d", err);
            return err;
        }
    }


    return 0;
}


int gps_get_single_fix() {
    int err;

    err = nrf_modem_gnss_fix_interval_set(0);
    if (err != 0) {
        LOG_ERR("Could not set fix interval: %d", err);
        return err;
    }
    err = nrf_modem_gnss_fix_retry_set(0);
    if (err != 0) {
        LOG_ERR("Could not set fix retry timeout: %d", err);
        return err;
    }

    err = nrf_modem_gnss_start();
    if (err != 0) {
        LOG_ERR("Could not start GNSS: %d", err);
        return err;
    }

    k_sem_take(&gps_got_fix, K_FOREVER);
    if(!got_fix_sucessfully) {
        LOG_ERR("Error when getting fix");
    } else {
        LOG_INF("Got a fix!");
        LOG_INF("long: %.6f, lat: %.6f, alt: %.6f, accuracy: %.4f", pvt_data.longitude, pvt_data.latitude, pvt_data.altitude, pvt_data.accuracy);
    }

    err = nrf_modem_gnss_stop();
    if (err != 0) {
        LOG_ERR("Could not stop GNSS: %d", err);
        return err;
    }

    return 0;

}

int gps_button_handler() {
    return 0;
}
