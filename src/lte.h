#ifndef TEST_LTE_H
#define TEST_LTE_H

#include <zephyr.h>

int lte_init();

int lte_block_until_ready();

void lte_button_handler();

int lte_do_transmission();

int lte_get_rrc_mode();

int lte_await_rrc_inactive(k_timeout_t timeout);

#endif
