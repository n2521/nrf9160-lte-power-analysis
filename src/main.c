/*
 * Copyright (c) 2019 Nordic Semiconductor ASA
 *
 * SPDX-License-Identifier: LicenseRef-Nordic-5-Clause
 */

#include <stdio.h>

#include <device.h>
#include <drivers/gpio.h>

#include <modem/lte_lc.h>
#include <dk_buttons_and_leds.h>
#include <zephyr.h>

#include <logging/log.h>

#include "lte.h"
#include "gps.h"

LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);

#define LED_SEND_DATA     DK_LED2
#define LED_ERROR         DK_LED3
#define LED_STARTED       DK_LED4

#define POWER_ANALYZER_TRIGGER_GPIO_DEVICE "GPIO_0"
#define POWER_ANALYZER_TRIGGER_PIN         21

typedef enum {
    TEST_LTE,
    TEST_GPS, 
} test_t;

static const struct device *dev;

int trigger_pin_init() {
    dev = device_get_binding(POWER_ANALYZER_TRIGGER_GPIO_DEVICE);
    return gpio_pin_configure(dev, POWER_ANALYZER_TRIGGER_PIN,
                                 GPIO_OUTPUT_HIGH | GPIO_ACTIVE_LOW);

}

void button_handler(uint32_t button_state, uint32_t has_changed) {
    if ((button_state & DK_BTN1_MSK) == 0) {
        lte_button_handler();
    } else if ((button_state & DK_BTN2_MSK) == 0) {
        gps_button_handler();
    }

}

void run_lte_tests() {
    int ret;
    lte_block_until_ready();
    dk_set_led_on(LED_SEND_DATA);
    ret = gpio_pin_set(dev, POWER_ANALYZER_TRIGGER_PIN, 1);
    if (ret != 0) {
        LOG_ERR("Could not set gpio pin(%d)", ret);
    }
    ret = lte_do_transmission();
    ret = gpio_pin_set(dev, POWER_ANALYZER_TRIGGER_PIN, 0);
    if (ret != 0) {
        LOG_ERR("Could not set gpio pin(%d)", ret);
    }
    dk_set_led_off(LED_SEND_DATA);
}

void main(void) {
    int ret;
    dk_leds_init();
    ret = trigger_pin_init();
    if(ret != 0) {
        LOG_ERR("Could not configure GPIO(%d)", ret);
        return;
    }

    dk_set_leds(DK_NO_LEDS_MSK);
    dk_set_led_on(DK_LED1);
    k_sleep(K_MSEC(100));
    dk_set_led_on(DK_LED2);
    k_sleep(K_MSEC(100));
    dk_set_led_on(DK_LED3);
    k_sleep(K_MSEC(100));
    dk_set_led_on(DK_LED4);
    k_sleep(K_MSEC(100));

    dk_set_leds(DK_NO_LEDS_MSK);

    dk_buttons_init(button_handler);

    ret = lte_init();
    if(ret != 0) {
        LOG_ERR("Could not initialize LTE");
        goto failure;
    }

    ret = gps_init();
    if (ret != 0) {
        LOG_ERR("Could not initialize GPS");
        goto failure;
    }

    dk_set_led_on(LED_STARTED);

#define TEST_TO_RUN TEST_GPS

    while (1) {
        switch(TEST_TO_RUN) {
            case TEST_LTE:
            lte_block_until_ready();
            lte_do_transmission();
                break;
            case TEST_GPS:
                lte_await_rrc_inactive(K_FOREVER);
                gps_get_single_fix();
                k_sleep(K_SECONDS(120));
                break;

        }
    }

failure:
    dk_set_leds(DK_NO_LEDS_MSK);
    dk_set_led_on(LED_ERROR);
    k_sleep(K_FOREVER);
}
