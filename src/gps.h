#ifndef TEST_GPS_H
#define TEST_GPS_H

int gps_init();
int gps_get_single_fix();
int gps_button_handler();

#endif
